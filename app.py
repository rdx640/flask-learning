from flask import Flask,jsonify, render_template
from flask_mail import Mail, Message
import requests
import os

app = Flask(__name__)
app.config['DEBUG'] = True
# app.config['TESTING'] = False
app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = os.getenv('EMAIL_USER')
app.config['MAIL_PASSWORD'] = os.getenv('EMAIL_PASS')
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

mail = Mail(app)

mailHtml="<h3>Server is down</h3> <style>h3{color:blue;}</style> <h1>Please check the server</h1> <style>h1{color:red;}</style>"



def check_endpoint():
    url = 'http://127.0.0.1:5000'
    try:
        response = requests.get(url)
        if response.status_code == 200:
            return True
        else:
            return False
    except requests.exceptions.ConnectionError:
        return False

def send_email():
    with app.app_context():
        subject = "Server is down Or Endpoint does not exist"
        # html = "<h3>Server is down</h3> "
        sender = app.config.get("MAIL_USERNAME")
        recipients = ["avijitsen.me@gmail.com"]
        msg = Message(subject, sender=sender, recipients=recipients)
        msg.body = "Server is down"
        mail.send(msg)

@app.route('/')
def index():
    # if check_endpoint():
    #     return jsonify({'status': 'Server is up'})
    # else:
    #     send_email()
    #     return jsonify({'status': 'Server is down'})
    return jsonify({'status': 'Server is up'})
    
@app.route('/monitor')
def monitor():
    return jsonify({'status': 'Monitoring'})

# current_ip
@app.route('/current_ip')
def current_ip():
    ip = requests.get('https://api.ipify.org').text
    return jsonify({'ip': ip})

#get current location
@app.route('/current_location')
def current_location():
    ip = requests.get('https://api.ipify.org').text
    url = 'http://ip-api.com/json/' + ip
    location = requests.get(url).json()
    return jsonify({'location': location})

# get current weather
@app.route('/current_weather')
def current_weather():
    ip = requests.get('https://api.ipify.org').text
    url = 'http://ip-api.com/json/' + ip
    location = requests.get(url).json()
    lat = location['lat']
    lon = location['lon']
    url = 'http://api.openweathermap.org/data/2.5/weather?lat=' + str(lat) + '&lon=' + str(lon) + '&appid=2b7e151e2d8b4e8e8f1d7f6c0e8c7e9f'
    weather = requests.get(url).json()
    return jsonify({'weather': weather})

# get current time
@app.route('/current_time')
def current_time():
    url = 'http://worldtimeapi.org/api/timezone/Asia/Kolkata'
    time = requests.get(url).json()
    return jsonify({'time': time})

#get a joke
@app.route('/joke')
def joke():
    url = 'https://official-joke-api.appspot.com/random_joke'
    joke = requests.get(url).json()
    return jsonify({'joke': joke})

# get a quote
@app.route('/quote')
def quote():
    url = 'https://api.quotable.io/random'
    quote = requests.get(url).json()
    return jsonify({'quote': quote})

# get a random image
@app.route('/image')
def image():
    url = 'https://picsum.photos/200/300'
    return jsonify({'image': url})

#get a random reddit post
@app.route('/reddit')
def reddit():
    url = 'https://www.reddit.com/r/aww/.json'
    reddit = requests.get(url).json()
    return jsonify({'reddit': reddit})



if __name__ == '__main__':
    app.run()